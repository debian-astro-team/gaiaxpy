numpy
pandas>=1.0.0
scipy
astropy
fastavro<=1.4.7
astroquery
matplotlib
tqdm>=4.64.0
